import sbt._
import Keys._
import xerial.sbt.Sonatype._


object Plugin extends Build {
  lazy val plugin = Project(
                    id = "plugin",
                    base = file("."),
                    settings = sonatypeSettings ++ sbtPluginSettings
                  )

  lazy val sbtPluginSettings = Seq(

    sbtPlugin := true,
    name := "sbt-templates",
    organization := "org.mollyware",
    shellPrompt <<= (name) { (name) => _ => name + "> " },
    logLevel := Level.Warn,

    incOptions := incOptions.value.withNameHashing(true),

    publishMavenStyle := true,
    publishArtifact in Test := false,

    publishTo := {
      val nexus = "https://oss.sonatype.org/"
      if (isSnapshot.value)
        Some("snapshots" at nexus + "content/repositories/snapshots")
      else
        Some("releases"  at nexus + "service/local/staging/deploy/maven2")
    },

    pomIncludeRepository := { x => false },
    licenses += ("Apache License, Version 2.0" -> url("http://www.apache.org/licenses/LICENSE-2.0.txt")),
    homepage := Some(url("http://www.mollyware.se")),
    scmInfo := Some(ScmInfo(url("https://bitbucket.org/mollyware/sbt-templates"),"https://bitbucket.org/mollyware/sbt-templates.git")),

    pomExtra := (
      <developers>
        <developer>
          <id>mollywarehq</id>
          <name>Mollyware</name>
        </developer>
      </developers>),

    scalacOptions ++= Seq(
      "-deprecation",
      "-unchecked",
      "-feature",
      "-language:postfixOps"
    ),

    resolvers ++=
      Seq(
        Resolver.mavenLocal,
        Resolver.sonatypeRepo("releases"),
        Resolver.url("Artifactory Online", url("http://scalasbt.artifactoryonline.com/scalasbt/repo"))(Resolver.ivyStylePatterns)
      )

  ) ++ Seq(
    addSbtPlugin("org.scalariform" % "sbt-scalariform" % "1.6.0"),
    addSbtPlugin("io.spray" % "sbt-revolver" % "0.8.0"),
    addSbtPlugin("com.typesafe.sbt" %% "sbt-web" % "1.3.0"),
    addSbtPlugin("com.typesafe.sbt" % "sbt-jshint" % "1.0.3"),
    addSbtPlugin("net.ground5hark.sbt" % "sbt-concat" % "0.1.8"),
    addSbtPlugin("net.ground5hark.sbt" % "sbt-css-compress" % "0.1.3"),
    addSbtPlugin("net.ground5hark.sbt" % "sbt-closure" % "0.1.3"),
    addSbtPlugin("com.slidingautonomy.sbt" % "sbt-filter" % "1.0.1"),
    addSbtPlugin("com.slidingautonomy.sbt" % "sbt-html-minifier" % "1.0.0"),
    addSbtPlugin("com.typesafe.sbt" % "sbt-less" % "1.1.0"),
    addSbtPlugin("com.typesafe.sbt" % "sbt-mocha" % "1.1.0"),
    addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.0.6")
  )
}
