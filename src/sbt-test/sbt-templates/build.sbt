import sbt._
import Defaults._
import org.mollyware.projects.Templates._
import org.mollyware.dependencies.DependencySet
import org.mollyware.dependencies.Dependencies

// CUSTOM DEPENDENCIES
import org.mollyware.dependencies.Dependencies._

/// FRONTENDS ///
lazy val frontendTest =
  FrontendProject( "frontend-test" ).
    settings( version := "0.0.1-SNAPSHOT" ).
    dependsOn( commonLib )

/// EXTERNAL SERVICES ///
lazy val serviceTest1 =
  WebStreamsProject( "service-external-test1" ).
    settings( version := "0.0.1-SNAPSHOT" ).
    dependsOn( commonLib )

lazy val serviceTest2 =
  WebStreamsProject( "service-external-test2" ).
    settings( version := "0.0.1-SNAPSHOT" ).
    dependsOn( commonLib )

/// INTERNAL SERVICES ///
lazy val serviceTest3 =
  StreamsProject( "service-internal-test3" ).
    settings( version := "0.0.1-SNAPSHOT" ).
    dependsOn( commonLib )

/// LIBRARIES ///

lazy val commonLib =
  LibraryProject( "lib-common" ).
    settings( version := "0.0.1-SNAPSHOT" )

scalaVersion := "2.11.8" // Needed for root project


/// AGGREGATION

lazy val frontends:Seq[ProjectReference] = Seq( frontendTest )
lazy val services:Seq[ProjectReference] = Seq(
    serviceTest1,
    serviceTest2,
    serviceTest3
  )
lazy val libraries:Seq[ProjectReference] = Seq( commonLib )
lazy val aggregates = frontends ++ services ++ libraries

lazy val root =
  project.in( file(".") ).
    aggregate( aggregates:_* )
