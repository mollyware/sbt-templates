package org.mollyware
package projects

import sbt._
import Keys._

object Templates {
  import dependencies.DependencySet._
  import dependencies.Dependencies
  import settings.Settings._
  import com.typesafe.sbt.web.SbtWeb
  import com.typesafe.sbt.packager.docker._
  import com.typesafe.sbt.packager.archetypes._
  import spray.revolver._

  private[this] def choosePath( name:String, path:Option[String], mustConvertDashToPath:Boolean ) = {
    def replaceWithPath:String = name.replace("-","/").replace("_", "/")
    def convertDashToPath:String =
        if(mustConvertDashToPath) replaceWithPath
        else name

    file( path.getOrElse( convertDashToPath ) )
  }

  def WebFrontendProject( name: String, path: Option[String] = None, mustConvertDashToPath:Boolean = true ) =
    Project( name, choosePath( name, path, mustConvertDashToPath ) ).
      settings( defaultSettings:_* ).
      settings( webUiSettings:_* ).
      enablePlugins( SbtWeb ).
      disablePlugins( RevolverPlugin )

  def WebStreamsProject( name: String, path: Option[String] = None, mustConvertDashToPath:Boolean = true ) =
    Project( name, choosePath( name, path, mustConvertDashToPath ) ).
      enablePlugins( JavaAppPackaging, DockerPlugin ).
      settings( defaultSettings:_* ).
      settings( libraryDependencies ++=
        akka ++
        akkaWeb ++
        streams ++
        persistence ++
        testing
      )

  def StreamsProject( name: String, path: Option[String] = None, mustConvertDashToPath:Boolean = true ) =
    Project( name, choosePath( name, path, mustConvertDashToPath ) ).
      settings( defaultSettings:_* ).
      settings( libraryDependencies ++=
        akka ++
        streams ++
        persistence ++
        testing
      )

  def LibraryProject( name: String, path: Option[String] = None, mustConvertDashToPath:Boolean = true ) =
    Project( name, choosePath( name, path, mustConvertDashToPath ) ).
    disablePlugins( RevolverPlugin ).
      settings( defaultSettings:_* ).
      settings( libraryDependencies ++=
        testing
      )
}
