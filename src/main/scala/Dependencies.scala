package org.mollyware
package dependencies

import sbt._
import Keys._

object Versions {
  val pickling   = "0.10.1"
  val akka       = "2.4.7"
  val scalatest  = "2.2.6"
  val scalacheck = "1.13.1"
  val bcastle    = "1.54"
  val jbcrypt    = "0.4"
  val cassandra  = "0.16"
  val inmem      = "1.2.13"
  val scalaLog   = "3.1.0"
}

object Dependencies {
  import Versions._

  // Serialization
  val picklings     = "org.scala-lang.modules" %% "scala-pickling"  % pickling

  // Akka
  val akkaActor    = "com.typesafe.akka" %% "akka-actor"           % akka
  val akkaRemote   = "com.typesafe.akka" %% "akka-remote"          % akka

  // Akka Experimental
  val akkaStreams         = "com.typesafe.akka" %% "akka-stream"                         % akka
  val akkaHttpCore        = "com.typesafe.akka" %% "akka-http-core"                      % akka
  val akkaHttp            = "com.typesafe.akka" %% "akka-http-experimental"              % akka

  // Persistence
  val akkaPersistence     = "com.typesafe.akka"     %% "akka-persistence"                    % akka
  val akkaPersistenceQ    = "com.typesafe.akka"     %% "akka-persistence-query-experimental" % akka
  val akkaCassandra       = "com.typesafe.akka"     %% "akka-persistence-cassandra"          % cassandra
  val akkaInMem           = "com.github.dnvriend"   %% "akka-persistence-inmemory"           % inmem      % "test"

  // Test
  val akkaTestkit         = "com.typesafe.akka" %% "akka-testkit"                        % akka       % "test"
  val akkaHttpTestKit     = "com.typesafe.akka" %% "akka-http-testkit"                   % akka       % "test"
  val akkaStreamsTestKit  = "com.typesafe.akka" %% "akka-stream-testkit"                 % akka       % "test"
  val scalaTest           = "org.scalatest"     %% "scalatest"                           % scalatest  % "test"
  val scalaCheck          = "org.scalacheck"    %% "scalacheck"                          % scalacheck % "test"

  // Codecs & Crypto
  val bcprov       = "org.bouncycastle"  % "bcprov-jdk15on"        % bcastle
  val bcpkix       = "org.bouncycastle"  % "bcpkix-jdk15on"        % bcastle
  val bcrypt       = "de.svenkubiak"     % "jBCrypt"               % jbcrypt
}

object DependencySet {
  import Dependencies._
  val testing     = Seq( scalaTest, scalaCheck )
  val streams     = Seq( akkaStreams, akkaStreamsTestKit )
  val akkaWeb     = Seq( akkaHttpCore, akkaHttp, akkaHttpTestKit, picklings )
  val persistence = Seq( akkaPersistence, akkaPersistenceQ, akkaCassandra, akkaInMem )
  val akka        = Seq( akkaActor, akkaRemote, akkaTestkit )
  val cryptos     = Seq( bcprov, bcpkix, bcrypt )
}
