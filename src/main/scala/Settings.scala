package org.mollyware
package settings

import sbt._
import Keys._

// SCALA
import scalariform.formatter.preferences._
import com.typesafe.sbt.SbtScalariform
import com.typesafe.sbt.SbtScalariform.ScalariformKeys
import spray.revolver.RevolverPlugin._

// SBT-WEB
import com.typesafe.sbt.web.Import._
import com.typesafe.sbt.web.Import.WebKeys._
import net.ground5hark.sbt.concat.Import._
import com.typesafe.sbt.jshint.Import._
import com.slidingautonomy.sbt.filter.Import._
import com.typesafe.sbt.less.Import._
import com.typesafe.sbt.mocha.Import._
import net.ground5hark.sbt.css.Import._
import net.ground5hark.sbt.closure.Import._
import com.slidingautonomy.sbt.htmlminifier.Import._

import java.text.SimpleDateFormat
import java.util.{Date, TimeZone}

object Settings {
  import PluginSettings._

  lazy val defaultSettings =
    formatSettings ++
    scalac ++
    javac ++
    jarSettings ++
    commonSettings ++
    repos ++
    prompt

  lazy val prompt = Seq(
      shellPrompt <<= (name) { (name) => _ => name + "> " }
    )

  import WebPluginSettings._

  lazy val webUiSettings =
    concatSettings ++
    filterSettings ++
    jshintSettings ++
    closureSettings ++
    cssCompressSettings ++
    webDistSettings ++
    pipeline
}

object WebPluginSettings {

  val concatSettings = Seq(
    Concat.parentDir := ""
  )

  lazy val webDist = taskKey[File]("Task that packages files in web staging directory to zip.")
  lazy val webDistFile = taskKey[File]("Path to write zip file to. Default is target/dist/<name>-<version>.zip.")

  val webDistSettings = Seq(
      webDistFile := webTarget.value / "dist" / s"${name.value}-${version.value}.zip",
      webDist := {
        val dir = stagingDirectory.value
        val out: File = webDistFile.value
        IO.zip(Path.allSubpaths(dir), out)
        out
      },
      webDist <<= webDist.dependsOn(stage)
    )

  val filterSettings = Seq(
    // We must filter out all subfolders before filtering parent folders.
    includeFilter in filter := "*.js" || "*.css" || "*.map" ||
                               "*.less" || "controllers" ||
                               "directives" || "filters" ||
                               "services" || "js" || "css" ||
                               "bootstrap-less" || "mixins" ||
                               "less" || "vendor",

    // Only allow concatenated files to slip through
    excludeFilter in filter := "app.js" || "vendor.js" || "app.css"
  )

  val jshintSettings = Seq(
    // JS Hint shoud not run on vendor
    excludeFilter in (Assets, JshintKeys.jshint) :=
      new FileFilter{
        def accept(f: File) = ".*/vendor/.*".r.pattern.matcher(f.getAbsolutePath).matches
      }
  )

  val closureSettings = Seq(
    Closure.flags := Seq(
      "--compilation_level=SIMPLE_OPTIMIZATIONS"
      ),
      // Closure should not run on vendor
      excludeFilter in closure :=
        new FileFilter{
          def accept(f: File) = ".*/vendor/.*".r.pattern.matcher(f.getAbsolutePath).matches
        }
    )

  val cssCompressSettings = Seq(
    excludeFilter in cssCompress :=
        new FileFilter{
          def accept(f: File) = ".*/vendor/.*".r.pattern.matcher(f.getAbsolutePath).matches
        }
    )

  def minifiyHtml( isSnapshot:Boolean ) =
    if( !isSnapshot ) Seq( htmlMinifier )
    else Seq.empty

  val pipeline = Seq(
    pipelineStages :=
      Seq(cssCompress, closure, concat, filter ) ++
      minifiyHtml( isSnapshot.value )
  )

}

object PluginSettings {
  val commonSettings = Seq(
    scalaVersion := "2.11.8",
    organization := "mollyware",
    version := "0.0.1-SNAPSHOT"
  )

  val commonScalacOptions = Seq(
    "-encoding",
    "UTF-8",
    "-deprecation",
    "-feature",
    "-language:postfixOps",
    "-language:implicitConversions",
    "-language:higherKinds",
    "-language:existentials",
    "-language:postfixOps",
    "-Ywarn-adapted-args",
    "-Ywarn-dead-code",
    "-Ywarn-numeric-widen",
    "-Ywarn-inaccessible",
    "-Yno-adapted-args",
    "-Ywarn-infer-any",
    "-Ywarn-unused-import",
    "-Yinline-warnings",
    "-unchecked",
    "-target:jvm-1.8"
  )

  val extraScalacOptions = Seq(
    "-Ywarn-value-discard"
  )

  // compile options
  lazy val scalac = Seq(
    scalacOptions in Compile := commonScalacOptions ++ extraScalacOptions,
    scalacOptions in Test := commonScalacOptions,
    scalacOptions <++= (version) map { v =>
      if (v.endsWith("SNAPSHOT")) Nil else Seq("-optimize")
    },
    incOptions := incOptions.value.withNameHashing(true)
  )

  def replaceWithDots(name: String) = name.replace("-",".").replace("_", ".")

  lazy val javac = Seq(
    javacOptions ++= Seq(
      "-Xlint:unchecked",
      "-Xlint:deprecation",
      "-Xlint",
      "-Xfuture"
    )
  )

  lazy val jarSettings = Seq( exportJars := true )

  lazy val repos = Seq(
    resolvers ++=
      Seq(
        Resolver.mavenLocal,
        Resolver.bintrayRepo("krasserm", "maven"),
        Resolver.bintrayRepo("dnvriend", "maven")
      )
    )

  lazy val formatSettings = SbtScalariform.scalariformSettings ++ Seq(
      ScalariformKeys.preferences in Compile := formattingPreferences,
      ScalariformKeys.preferences in Test    := formattingPreferences
    )

  lazy val formattingPreferences = FormattingPreferences()
    .setPreference( RewriteArrowSymbols, true )
    .setPreference( AlignParameters, true )
    .setPreference( AlignArguments, true )
    .setPreference( AlignSingleLineCaseStatements, true )
    .setPreference( SpaceInsideParentheses, true )
    .setPreference( FormatXml, true )
    .setPreference( DoubleIndentClassDeclaration, true )
    .setPreference( IndentLocalDefs, true )
    .setPreference( SpacesAroundMultiImports, true )
}
