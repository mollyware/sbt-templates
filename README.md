# Mollyware SBT Templates

[![wercker status](https://app.wercker.com/status/79cabbaed332c8b627bb0c30e9bc676f/m/ "wercker status")](https://app.wercker.com/project/bykey/79cabbaed332c8b627bb0c30e9bc676f)

Standard project templates setup from Mollyware

## Installing

Example `/project/plugin.sbt`

```scala
import sbt._

import Defaults._

resolvers += Resolver.typesafeRepo("releases")

resolvers += "Sonatype releases" at "https://oss.sonatype.org/content/repositories/releases/"

addSbtPlugin("org.mollyware" % "sbt-templates" % "0.0.10")
```

In your root dir add one or more of your template projects
```scala
import sbt._

import Defaults._

import org.mollyware.projects.Templates._

import org.mollyware.dependencies.DependencySet._

lazy val rest-project = WebRestProject( "rest-project" ).
                    dependsOn( domain )

lazy val messages = LibraryProject( "messages" ).
                      dependsOn( domain )

lazy val domain = LibraryProject( "domain" )
  .settings( libraryDependencies ++= timeCodec ) // This is how you add extra dependencies

lazy val internal = InternalServiceProject( "internal" ) 

lazy val uiweb = WebFrontendProject( "uiweb" )

scalaVersion := "2.11.7" // Needed for root project

lazy val root =
  project.in( file(".") ).
    aggregate( rest-project, messages, internal, domain, uiweb )

```


## Features

Project Templates for SBT.

Contains several standard project templates with default settings and plugins

##### WebFrontendProject
AngularJS for Web UI:s, procudes a zip-file.

##### WebRestProject
Spray.io and Akka for REST project, produces an executable zip file or a docker-file

##### LibraryProject
Standalone libraries, produces a jar.

##### InternalServiceProject
Same as WebRest project but without Spray.io.

### Monitoring

To setup Kamon monitoring for Newrelic and spray you need to edit some configuration.

#### Add monitoring to your project

You need to add the monitoring library to your project in your `build.sbt`.
```scala
import org.mollyware.dependencies.DependencySet._

lazy val my-project = WebRestProject( "my-project" ).
                    dependsOn( ... ).
                    settings( libraryDependencies ++= monitoring )

lazy val root =
  project.in( file(".") ).
    aggregate( my-project )
```

Configure kamon settings for New Relic in `application.conf`.

```scala
kamon {
  newrelic {
    // These values must match the values present in your newrelic.yml file.
    app-name = <your-app-name>
    license-key = <your-newrelic-license>
  }

  spray {
    include-trace-token-header = true
    trace-token-header-name = "X-Trace-Token"
  }
}
```

Also add Kamon to Akka extensions in your `application.conf` file.

```scala
akka {
  extensions = ["kamon.metric.Metrics"]
}
``